
(function ($) {

/**
 * Provide summary information for vertical tabs.
 */
Drupal.behaviors.revisions_scheduler_settings = {
  attach: function (context) {

  // Add the theme name as an additional class to the vertical-tabs div. This can then be used
  // in scheduler.css to rectify the style for collapsible fieldsets where different themes
  // need slightly different fixes. The theme is available in ajaxPageState.
  var theme = Drupal.settings.ajaxPageState['theme'];
  $("div.vertical-tabs").addClass(theme);

	// Provide summary when editting a node.
	$('fieldset#edit-revisions-scheduler-settings', context).drupalSetSummary(function(context) {
      var vals = [];
      if ($('#edit-revision-publish-on').val() || $('#edit-revision-publish-on-datepicker-popup-0').val()) {
        vals.push(Drupal.t('Scheduled for publishing'));
      }
      if ($('#edit-revision-unpublish-on').val() || $('#edit-revision-unpublish-on-datepicker-popup-0').val()) {
        vals.push(Drupal.t('Scheduled for unpublishing'));
      }
      if (!vals.length) {
        vals.push(Drupal.t('Not scheduled'));
      }
      return vals.join('<br/>');
    });

    // Provide summary during content type configuration.
    $('fieldset#edit-revisions-scheduler', context).drupalSetSummary(function(context) {
      var vals = [];
      if ($('#edit-revisions-scheduler-publish-enable', context).is(':checked')) {
        vals.push(Drupal.t('Publishing enabled'));
      }
      if ($('#edit-revisions-scheduler-unpublish-enable', context).is(':checked')) {
        vals.push(Drupal.t('Unpublishing enabled'));
      }
      return vals.join('<br/>');
    });

  }
};

})(jQuery);
