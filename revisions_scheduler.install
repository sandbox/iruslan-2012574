<?php

/**
 * @file
 * Installation file for Revisions scheduler module.
 */

/**
 * Implements hook_schema().
 */
function revisions_scheduler_schema() {
  return array(
    'revisions_scheduler' => array(
      'description' => 'The main table to hold the scheduler data.',
      'fields' => array(
        'nid' => array(
          'description' => 'The foreign key to node.nid',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'vid' => array(
          'description' => 'The primary identifier for this version.',
          'type' => 'serial',
          'unsigned' => TRUE,
          'not null' => TRUE,
        ),
        'revision_publish_on' => array(
          'description' => 'The UNIX UTC timestamp when to publish',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ),
        'revision_unpublish_on' => array(
          'description' => 'The UNIX UTC timestamp when to unpublish',
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'default' => 0,
        ),
      ),
      'indexes' => array(
        'nid' => array('nid'),
        'revisions_scheduler_publish_on' => array('revision_publish_on'),
        'revisions_scheduler_unpublish_on' => array('revision_unpublish_on'),
      ),
      'primary key' => array('vid'),
    ),
  );
}

/**
 * Implements hook_uninstall().
 */
function revisions_scheduler_uninstall() {
  $variables = array(
    'revisions_scheduler_date_format',
    'revisions_scheduler_field_type',
    'revisions_scheduler_extra_info',
  );

  $types = node_type_get_types();
  foreach ($types as $type) {
    $type_name = $type->type;
    $variables[] = "revisions_scheduler_publish_enable_" . $type_name;
    $variables[] = "revisions_scheduler_publish_touch_" . $type_name;
    $variables[] = "revisions_scheduler_publish_required_" . $type_name;
    $variables[] = "revisions_scheduler_publish_revision_" . $type_name;
    $variables[] = "revisions_scheduler_unpublish_enable_" . $type_name;
    $variables[] = "revisions_scheduler_unpublish_required_" . $type_name;
    $variables[] = "revisions_scheduler_unpublish_revision_" . $type_name;
  }

  foreach ($variables as $variable) {
    variable_del($variable);
  }
}
